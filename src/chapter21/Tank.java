package chapter21;

public class Tank {
	enum State { EMPTY, LOADED};
	private State state = State.EMPTY;
	private int current_load = 0;
	public void validate() {
		if((state == State.EMPTY && current_load != 0) ||
				(state == State.LOADED && current_load == 0)) {
			throw new IllegalStateException();
		}
	}
	public void fill() {
		state = State.LOADED;
		Thread.yield();
		current_load = 10;
	}
	public void drain() {
		state = State.EMPTY;
		Thread.yield();
		current_load = 0;
	}
}
