package chapter21;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class E14_ManyTimers {
	public static void main(String[] args) throws Exception {
		if(args.length < 1) {
			System.err.println(
					"Usage: java E14_ManyTimers <num of timers>");
		}
		int numOfTimers = Integer.parseInt(args[0]);
		for (int i = 0; i < args.length; i++) {
			new Timer().schedule(new TimerTask() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					System.out.println(System.currentTimeMillis());
				}
			}, numOfTimers - i);
		}
		TimeUnit.MILLISECONDS.sleep(2 * numOfTimers);
		System.exit(0);
	}
}
