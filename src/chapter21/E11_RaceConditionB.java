package chapter21;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class E11_RaceConditionB {
	public static void main(String[] args) {
		Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
		System.out.println("Press Control-C to exit");
		ExecutorService exec = Executors.newCachedThreadPool();
		SafeTank tank = new SafeTank();
		for (int i = 0; i < 10; i++)
			exec.execute(new ConsistencyChecker(tank));
		Thread.yield();
		exec.shutdown();
	}
}
