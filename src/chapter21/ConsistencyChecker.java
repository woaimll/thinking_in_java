package chapter21;

import java.util.Random;

public class ConsistencyChecker implements Runnable{
	private static Random rnd = new Random();
	private Tank tank;
	ConsistencyChecker(Tank tank){
		this.tank = tank;
	}
	@Override	
	public void run() {
		// TODO Auto-generated method stub
		for(;;) {
			if(rnd.nextBoolean()) {
				tank.fill();
			}else {
				tank.drain();
			}
			tank.validate();
		}
	}

}
