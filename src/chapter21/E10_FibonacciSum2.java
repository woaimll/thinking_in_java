package chapter21;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class E10_FibonacciSum2 {
	public static void main(String[] args) {
		ArrayList<Future<Integer>> results =
				new ArrayList<>();
		FibonacciSum2.init();
		for (int i = 0; i < 5; i++) {
			results.add(FibonacciSum2.runTask(i));
		}
		Thread.yield();
		FibonacciSum2.shutdown();
		for(Future<Integer> fi : results) {
			try {
				System.out.println(fi.get());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
