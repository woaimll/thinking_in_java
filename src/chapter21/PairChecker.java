package chapter21;

public class PairChecker implements Runnable{
	private PairManager pm;
	public PairChecker(PairManager pm) {
		this.pm = pm;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true) {
			pm.checkCounter.incrementAndGet();
			pm.getPair().checkState();
		}
	}

}
