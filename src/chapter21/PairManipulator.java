package chapter21;

public class PairManipulator implements Runnable{
	private PairManager pm;
	public PairManipulator(PairManager pm) {
		this.pm = pm;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true) {
			pm.increment();
		}
	}
	public String toString() {
		return "Pair: " + pm.getPair() +
				" checkCounter = " + pm.checkCounter.get();
	}

}
